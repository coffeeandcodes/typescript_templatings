import { useEffect, useState } from 'react';

interface windowResizeReturnType {
    windowWidth: number,
    windowHeight: number
}

const useWindowResize = (): windowResizeReturnType => {
    const [dimensions, setDimensions] = useState({
        height: window.innerHeight,
        width: window.innerWidth
    })

    useEffect(() => {
        function handleResize() {
            setDimensions({
                height: window.innerHeight,
                width: window.innerWidth
            })
        }

        window.addEventListener('resize', handleResize);
        return () => window.removeEventListener('resize', handleResize);
    })

    return ({
        windowWidth: dimensions.width,
        windowHeight: dimensions.height
    })
}

export default useWindowResize;