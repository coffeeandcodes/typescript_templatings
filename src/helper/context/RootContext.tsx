import { createContext } from 'react';

type ContextProps = {
  isRootMenuOpen: boolean,
  isMailMenuOpen: boolean,
  toggleRootMenu: () => void,
  toggleMailMenu: () => void,
  windowWidth: number
};

const RootContext = createContext({} as ContextProps);

export default RootContext;