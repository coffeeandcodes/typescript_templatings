import React, { useState } from 'react';
import RootContext from "./RootContext";
import useWindowResize from '../hooks/useWindowResize';

const Provider: React.FC = (props) => {
    const [isRootMenuOpen, setIsRootMenuOpen] = useState(false);
    const [isMailMenuOpen, setIsMailMenuOpen] = useState(true);
    const { windowWidth } = useWindowResize();

    function toggleRootMenu() {
        setIsRootMenuOpen(prevState => !prevState);
    }

    function toggleMailMenu() {
        setIsMailMenuOpen(prevState => !prevState);
    }

    return (
        <RootContext.Provider
            value={{
                isRootMenuOpen, toggleRootMenu,
                isMailMenuOpen, toggleMailMenu,
                windowWidth
            }}
        >
            {props.children}
        </RootContext.Provider>
    )
}

export default Provider;