import React from 'react';
import './styles/button.scss';

interface Props {
  title?: string,
  customTitle?: React.ReactNode
}

const Button = (props: Props) => {
  const { title, customTitle } = props;
  return (
    <button
      type="button"
      className="custom-button">{customTitle ?? title}</button>
  )
}

export default Button
