import React from 'react';
import './style/RadioButton.scss';

interface Props {
    borderColor?: string,
    activeColor?: string,
    disabled?: boolean,
    value?: boolean,
    onChange?: (value: boolean) => void,
    containerStyle?: React.CSSProperties,
    innerContainerStyle?: React.CSSProperties,
    activeBackGround?: string,
    inactiveBackground?: string,
    size: number,
    label?: string
}

const RadioButton = (props: Props) => {
    const { activeColor, borderColor, disabled, value, onChange, containerStyle,
        activeBackGround, inactiveBackground, size, label, innerContainerStyle } = props;
    return (
        <div
            style={containerStyle}
            className="radio-button-outer">
            <div
                onClick={() => disabled ? null : onChange ? onChange(!value) : null}
                style={{
                    opacity: disabled ? 0.6 : 1, backgroundColor: value ? activeBackGround : inactiveBackground,
                    borderColor: activeBackGround !== 'white' ? activeBackGround : borderColor,
                    width: size + 10, height: size + 10, ...innerContainerStyle
                }}
                className="custom-radio-button"
            >
                {value && <div
                    style={{ backgroundColor: activeColor, width: size, height: size }}
                    className="radio-center" />}
            </div>
            {label && <p className="radio-button-label">{label}</p>}
        </div>
    )
}

RadioButton.defaultProps = {
    activeColor: '#3B86FF',
    borderColor: '#3B86FF',
    activeBackGround: 'white',
    inactiveBackground: 'white',
    size: 15
}

export default RadioButton
