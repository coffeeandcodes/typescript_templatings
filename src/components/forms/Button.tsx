import React from 'react';
import './style/Button.scss';

interface Props {
    title?: string,
    customTitle?: React.ReactNode,
    backgroundcolor?: string,
    color?: string,
    bordercolor?: string,
    containerstyle?: React.CSSProperties,
    disabled?: boolean
}

const Button = (props: Props & React.HTMLAttributes<HTMLButtonElement>) => {
    const { title, customTitle, backgroundcolor, color, bordercolor, disabled, containerstyle } = props;
    return (
        <button
            style={{
                backgroundColor: backgroundcolor, color, borderColor: bordercolor, borderWidth: backgroundcolor ? 0 : 1,
                opacity: disabled ? 0.6 : 1, outlineWidth: (disabled || backgroundcolor) ? 0 : 2,
                outlineColor: bordercolor, ...containerstyle
            }}
            className="custom-buttons"
            {...props}
        >{customTitle ?? title}</button>
    )
}

Button.defaultProps = {
    color: '#FFFFFF',
    bordercolor: '#43425D'
}

export default Button
