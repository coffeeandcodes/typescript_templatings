import React, { ReactElement } from 'react';
import './style/CheckBox.scss';
import { ReactComponent as CheckIcon } from "../../assets/icons/check.svg";

interface Props {
    disabled?: boolean,
    value?: boolean,
    onChange?: (value: boolean) => void,
    containerStyle?: React.CSSProperties,
    innerContainerStyle?: React.CSSProperties,
    borderColor?: string,
    activeBackGround?: string,
    inactiveBackground?: string,
    tickColor?: string,
    size: number,
    label?: string
}

function CheckBox(props: Props): ReactElement {
    const { disabled, value, containerStyle, onChange, borderColor, activeBackGround,
        inactiveBackground, tickColor, size, label, innerContainerStyle } = props;
    return (
        <div
            style={containerStyle}
            className="outer-container">
            <div
                onClick={() => disabled ? null : onChange ? onChange(!value) : null}
                style={{
                    opacity: disabled ? 0.6 : 1, borderColor: activeBackGround !== 'white' ? activeBackGround : borderColor,
                    backgroundColor: value ? activeBackGround : inactiveBackground, width: size + 6, height: size + 6,
                    ...innerContainerStyle
                }}
                className="custom-checkbox">
                {value && <CheckIcon fill={tickColor} width={size} height={size} className="check-icon" />}
            </div>
            {label && <p className="checkbox-label">{label}</p>}
        </div>
    )
}

CheckBox.defaultProps = {
    borderColor: "#3B86FF",
    activeBackGround: 'white',
    inactiveBackground: 'white',
    tickColor: "#3B86FF",
    size: 20
}

export default CheckBox;