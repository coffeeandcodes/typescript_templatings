import React, { ReactElement } from 'react'
import './style/Switch.scss';
import { ReactComponent as CheckIcon } from "../../assets/icons/check.svg";
import { ReactComponent as CrossIcon } from "../../assets/icons/plus.svg";


interface Props {
    value?: boolean,
    disabled?: boolean,
    onChange?: (value: boolean) => void,
    containerStyle?: React.CSSProperties,
    switchButtonStyle?: React.CSSProperties,
    mode?: string
}

function Switch(props: Props): ReactElement {
    const { disabled, onChange, value, containerStyle, switchButtonStyle, mode } = props;
    return (
        <div
            onClick={() => disabled ? null : onChange ? onChange(!value) : null}
            style={{ opacity: disabled ? 0.6 : 1, ...containerStyle }}
            className={`custom-switch ${value ? 'custom-switch-open' : ''}`}>
            <div className={`switch-indicator ${value ? 'switch-indicator-open' : ''}`}>
                {mode === "text" && <p className="switch-text">{value ? 'ON' : 'OFF'}</p>}
                {mode === "icon" && <>
                    {value ? <CheckIcon className="switch-icon" /> : <CrossIcon className="cross-icon" />}
                </>}
            </div>
            <div style={switchButtonStyle} className="switch-button"></div>
        </div>
    )
}

export default Switch;