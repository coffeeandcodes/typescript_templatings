import React, { useState } from 'react'
import './style/TextBox.scss';

interface Props {
    label?: string,
    containerstyle?: React.CSSProperties,
    error?: any,
    rightInner?: React.ReactNode,
    wrapperStyle?: React.CSSProperties,
    borderColor?: string
}

function errorMessage(error: any) {
    switch (error.type) {
        case "required":
            return 'Field is required';
        default:
            return error.message;
    }
}


const TextBox = React.forwardRef<HTMLInputElement, Props & React.HTMLProps<HTMLInputElement>>((props, ref) => {
    const { label, containerstyle, error, rightInner, borderColor, wrapperStyle } = props;
    const [isFocused, setIsFocused] = useState(false);

    function toggleFocus() {
        setIsFocused(!isFocused);
    }

    return (
        <div
            style={wrapperStyle}
            className="textbox-outer">
            {label && <p className="label">{label}</p>}

            <div
                style={{ borderColor: error ? 'red' : isFocused ? '#5850ec' : borderColor, ...containerstyle }}
                className={`textbox-inner ${isFocused ? 'focus' : ''}`}>
                <input
                    onFocus={toggleFocus}
                    onBlur={toggleFocus}
                    className="textbox"
                    type="text"
                    ref={ref}
                    {...props}
                />
                {rightInner ?? null}
            </div>
            {error && <p className="error-message">{errorMessage(error)}</p>}

        </div>
    )
})

TextBox.defaultProps = {
    borderColor: '#cfd8e3'
}


export default TextBox;