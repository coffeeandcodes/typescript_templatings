import React, { useState } from 'react'
import './style/TextBox.scss';

interface Props {
    label?: string,
    containerstyle?: React.CSSProperties,
    outerContainerStyle?: React.CSSProperties,
    error?: any,
    rightInner?: React.ReactNode,
    borderColor?: string
}

function errorMessage(error: any) {
    switch (error.type) {
        case "required":
            return 'Field is required';
        default:
            return error.message;
    }
}


const TextArea = React.forwardRef<HTMLTextAreaElement, Props & React.TextareaHTMLAttributes<HTMLTextAreaElement>>((props, ref) => {
    const { label, containerstyle, error, rightInner, borderColor, outerContainerStyle } = props;
    const [isFocused, setIsFocused] = useState(false);

    function toggleFocus() {
        setIsFocused(!isFocused);
    }

    return (
        <div
            style={outerContainerStyle}
            className="textbox-outer">
            {label && <p className="label">{label}</p>}

            <div
                style={{ borderColor: error ? 'red' : isFocused ? '#5850ec' : borderColor, ...containerstyle }}
                className={`textbox-inner ${isFocused ? 'focus' : ''}`}>
                <textarea
                    onFocus={toggleFocus}
                    onBlur={toggleFocus}
                    className="textbox"
                    ref={ref}
                    rows={4}
                    {...props}
                />
                {rightInner ?? null}
            </div>
            {error && <p className="error-message">{errorMessage(error)}</p>}

        </div>
    )
})

TextArea.defaultProps = {
    borderColor: '#cfd8e3'
}


export default TextArea;