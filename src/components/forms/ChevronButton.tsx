import React, { ReactElement } from 'react';
import './style/ChevronButton.scss';
import { ReactComponent as ChevronRight } from "../../assets/icons/chevron-right.svg";

interface Props {
    direction?: string,
    customicon?: React.FunctionComponent<React.SVGProps<SVGSVGElement>>,
    size?: number 
}

function ChevronButton(props: Props & React.HTMLAttributes<HTMLButtonElement>): ReactElement {
    const { direction, customicon, size } = props;
    const Icon = customicon ?? ChevronRight;
    return (
        <button
            className="chevron-custom-button"
            {...props}
        >
            <Icon
                style={{ width: size, height: size, transform: `rotate(${direction === 'left' ? '0' : '180'}deg)` }}
                className="chevronIcon" />
        </button>
    )
}

ChevronButton.defaultProps = {
    direction: 'left',
    size: 24
}


export default ChevronButton;