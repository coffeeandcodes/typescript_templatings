import React, { useContext } from 'react';
import { NavLink } from 'react-router-dom';

import RootContext from '../../helper/context/RootContext';
import { ReactComponent as MailIcon } from "../../assets/icons/mail.svg";
import { ReactComponent as FinanceIcon } from "../../assets/icons/finance.svg";
import { ReactComponent as FormIcon } from "../../assets/icons/forms.svg";
import userAvatar from "../../assets/images/user-avatar.jpg";

import "./style/RootSidebar.scss";

interface List {
    id: number,
    label: string,
    Icon: React.FunctionComponent<React.SVGProps<SVGSVGElement>>,
    rightLabel?: number,
    iconColor: string,
    route: string
}

const routeList: List[] = [
    { id: 0, label: 'finance', Icon: FinanceIcon, iconColor: "#97a6ba", route: 'finance' },
    { id: 1, label: 'mailbox', Icon: MailIcon, iconColor: "#97a6ba", route: 'mailbox' },
    { id: 2, label: 'layouts', Icon: FormIcon, iconColor: "#97a6ba", route: 'layouts' },
]


const RootSidebar = () => {
    const { isRootMenuOpen, toggleRootMenu } = useContext(RootContext);

    return (
        <>
            <div className={`root-sidebar ${isRootMenuOpen ? 'root-sidebar-open' : ''}`}>

                <div className="user-container">
                    <img src={userAvatar} alt="user-avatar" className="user-avatar" />
                    <span className="text-primary ">Andrew Watkins</span>
                    <span className="text-primary email">watkins.andrew@company.com</span>
                </div>

                {routeList.map(route => <NavLink
                    activeClassName="list-container active"
                    className="list-container"
                    key={route.id}
                    to={`/${route.route}`}>
                    <route.Icon fill={route.iconColor} className="label-icon" />
                    <p className="label-name">{route.label}</p>
                </NavLink>)}


            </div>

            {isRootMenuOpen && <div
                onClick={toggleRootMenu}
                className="root-sidebar-overlay" />}
        </>
    )
}

export default RootSidebar;
