import React, { useContext } from 'react';
import { NavLink } from 'react-router-dom';

import RootContext from '../../helper/context/RootContext';
import { Button } from '../../helper/forms';
import { ReactComponent as PlusIcon } from "../../assets/icons/plus.svg";

import { filterList, labelList, mailboxList, List } from "../../route/routeData";
import "./style/MailSidebar.scss";


const MailSideBar = () => {
    const { isMailMenuOpen, toggleMailMenu } = useContext(RootContext);

    return (
        <>
            <div className={`mail-sidebar ${isMailMenuOpen ? 'mail-sidebar-open' : ''}`}>

                <h1 className="title">Mailbox</h1>
                <Button
                    customTitle={
                        <>
                            <PlusIcon fill="white" height="18" width="18" />
                            <span className="composeTitle">Compose</span>
                        </>
                    }
                />

                <CustomList
                    label="MAILBOXES"
                    labelList={mailboxList}
                />

                <CustomList
                    label="FILTERS"
                    labelList={filterList}
                />

                <CustomList
                    label="LABELS"
                    labelList={labelList}
                />
            </div>

            {isMailMenuOpen && <div
                onClick={toggleMailMenu}
                className="mail-sidebar-overlay" />}
        </>
    )
}

export default MailSideBar;

interface CustomListProps {
    label: string,
    labelList: List[]
}

const CustomList = (props: CustomListProps) => {
    const { label, labelList } = props
    return <>
        <h3 className="label-title">{label}</h3>

        {labelList.map(list => <NavLink
            activeClassName="list-container active"
            className="list-container"
            key={list.id}
            to={`/mailbox/${list.route}`}>
            <div className="inner-list-container">
                <list.Icon fill={list.iconColor} className="label-icon" />
                <p className="label-name">{list.label}</p>
            </div>
            <p className="right-label">{list.rightLabel}</p>
        </NavLink>)}
    </>
}