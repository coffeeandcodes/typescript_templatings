import React, { useContext } from 'react';
import RootContext from '../../helper/context/RootContext';
import { ReactComponent as HamburgerIcon } from "../../assets/icons/hamburger.svg";
import { ReactComponent as SearchIcon } from "../../assets/icons/search.svg";
import { ReactComponent as BookMarkIcon } from "../../assets/icons/bookmark.svg";
import { ReactComponent as MessageIcon } from "../../assets/icons/message.svg";
import './style/RootHeader.scss';

let actionList = [SearchIcon, BookMarkIcon, MessageIcon];

const RoottHeader: React.FC = () => {
    const { toggleRootMenu } = useContext(RootContext);
    return (
        <div className="root-header">
            <button
                onClick={toggleRootMenu}
                className="button hambruger">
                <HamburgerIcon className="button-icon" />
            </button>

            <div className="button-container">
                {actionList.map((Icon, index) => <button
                    className="button action-button"
                    key={index}>
                    <Icon className="button-icon" />
                </button>)}
            </div>
        </div>
    )
}

export default RoottHeader;