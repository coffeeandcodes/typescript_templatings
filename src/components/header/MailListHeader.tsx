import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import "./style/MailListHeader.scss";

import { ReactComponent as HamburgerIcon } from "../../assets/icons/hamburger.svg";
import { ReactComponent as ChevronRight } from "../../assets/icons/chevron-right.svg";
import RootContext from '../../helper/context/RootContext';

interface Props {

}

const MailListHeader: React.FC<Props> = () => {
    const { toggleMailMenu } = useContext(RootContext);
    const history = useHistory();
    const routeName = history.location.pathname.replace('/mailbox/', '');

    return (
        <div className="navigation-container">
            <div className="left-container">
                <button
                    onClick={toggleMailMenu}
                    className="button hambruger">
                    <HamburgerIcon className="button-icon" />
                </button>
                <p className="label-title">{routeName}</p>
            </div>

            <div className="right-container">
                <span>21</span>
                <span className="seperator">-</span>
                <span>30</span>
                <span className="seperator">of</span>
                <span>45</span>

                <div className="button-container">
                    <button
                        className="button chevron-button">
                        <ChevronRight className="button-icon chevron" />
                    </button>
                    <button
                        className="button chevron-button">
                        <ChevronRight className="button-icon chevron right" />
                    </button>
                </div>
            </div>
        </div>
    );
}

export default MailListHeader;