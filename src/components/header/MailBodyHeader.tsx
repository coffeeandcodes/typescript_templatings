import React from 'react';
import './style/mailbodyheader.scss';
import { ReactComponent as StarredIcon } from "../../assets/icons/starred.svg";
import { ReactComponent as LabelIcon } from "../../assets/icons/label.svg";
import { ReactComponent as MoreIcon } from "../../assets/icons/more.svg";
import { ReactComponent as ImportantIcon } from "../../assets/icons/important.svg";
import { ReactComponent as BackArrowIcon } from "../../assets/icons/back-arrow.svg";

interface MailList {
    isImportant?: boolean,
    isStarred?: boolean,
}

interface Props {
    selectedList: MailList,
    setSelectedList: React.Dispatch<React.SetStateAction<{}>>
}

const MailBodyHeader: React.FC<Props> = ({ setSelectedList, selectedList }) => {
    return (
        <div className="mailbody-header">
            <button
                onClick={() => setSelectedList({})}
                className="icon-button back">
                <BackArrowIcon
                    fill="#64748b"
                    className="header-icon" />
            </button>
            <div>
                <button className="icon-button">
                    <LabelIcon
                        fill="#64748b"
                        className="header-icon" />
                </button>
                <button className="icon-button">
                    <ImportantIcon
                        fill={selectedList.isImportant ? "#f05252" : "#64748b"}
                        className="header-icon" />
                </button>
                <button className="icon-button">
                    <StarredIcon
                        fill={selectedList.isStarred ? "#f05252" : "#64748b"}
                        className="header-icon" />
                </button>
                <button className="icon-button">
                    <MoreIcon
                        fill="#64748b"
                        className="header-icon" />
                </button>
            </div>
        </div>
    );
}

export default MailBodyHeader;