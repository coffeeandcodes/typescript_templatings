import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
// import MailRoute from './route/MailRoute';
import Provider from './helper/context/Provider';
import RootRoute from './route/RootRoute';

ReactDOM.render(
  // <React.StrictMode>
    <Provider>
      <RootRoute />
    </Provider>,
  // </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
