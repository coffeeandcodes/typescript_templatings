import React, { useContext } from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';

import "./style/RootRoute.scss";
import Finance from '../templates/finance/Finance';
import MailRoute from './MailRoute';
import RootSidebar from '../components/sidebar/RootSidebar';
import RootContext from '../helper/context/RootContext';
import RoottHeader from '../components/header/RootHeader';
import Layouts from '../templates/Layouts/Layouts';

const RootRoute: React.FC = () => {
    const { isRootMenuOpen } = useContext(RootContext);

    return (
        <Router>
            <div className="root-container">
                <RootSidebar />

                <div className={`root-body ${isRootMenuOpen ? "root-body-open" : ""}`}>
                    <RoottHeader />

                    <Route exact path="/" render={() => (
                        <Redirect to="/finance" />
                    )} />
                    <Route path={`/finance`} component={Finance} />
                    <Route path={`/mailbox`} component={MailRoute} />
                    <Route path={`/layouts`} component={Layouts} />
                </div>
            </div>
        </Router>
    )
}

export default RootRoute;