import React, { useContext } from 'react'
import { Route, Redirect } from 'react-router-dom';
import Mailbox from '../templates/mailbox/Mailbox';
import MailSideBar from '../components/sidebar/MailSideBar';
import './style/MailRoute.scss';

import { filterList, labelList, mailboxList } from "./routeData";
import RootContext from '../helper/context/RootContext';


const MailRoute: React.FC = () => {
    const { isMailMenuOpen } = useContext(RootContext);

    return (
        <div className="mail-container">

            <MailSideBar />

            <div className={`mail-body ${isMailMenuOpen ? 'mail-menu-open' : ''}`}>

                <Route exact path="/mailbox" render={() => (
                    <Redirect to="/mailbox/inbox" />
                )} />

                {mailboxList.map(route => <Route
                    key={route.id}
                    path={`/mailbox/${route.route}`}
                    component={Mailbox} />)}

                {filterList.map(route => <Route
                    key={route.id}
                    path={`/mailbox/${route.route}`}
                    component={Mailbox} />)}

                {labelList.map(route => <Route
                    key={route.id}
                    path={`/mailbox/${route.route}`}
                    component={Mailbox} />)}

            </div>
        </div>
    )
}

export default MailRoute;