import { ReactComponent as InboxIcon } from "../assets/icons/inbox.svg";
import { ReactComponent as SentIcon } from "../assets/icons/sent.svg";
import { ReactComponent as DraftIcon } from "../assets/icons/draft.svg";
import { ReactComponent as SpamIcon } from "../assets/icons/spam.svg";
import { ReactComponent as TrashIcon } from "../assets/icons/trash.svg";

import { ReactComponent as StarredIcon } from "../assets/icons/starred.svg";
import { ReactComponent as ImportantIcon } from "../assets/icons/important.svg";
import { ReactComponent as LabelIcon } from "../assets/icons/label.svg";

export interface List {
    id: number,
    label: string,
    Icon: React.FunctionComponent<React.SVGProps<SVGSVGElement>>,
    rightLabel?: number,
    iconColor: string,
    route: string
}

const mailboxList: List[] = [
    { id: 0, label: 'Inbox', Icon: InboxIcon, rightLabel: 27, iconColor: "#97a6ba", route: 'inbox' },
    { id: 1, label: 'Sent', Icon: SentIcon, iconColor: "#97a6ba", route: 'sent' },
    { id: 2, label: 'Drafts', Icon: DraftIcon, rightLabel: 7, iconColor: "#97a6ba", route: 'drafts' },
    { id: 3, label: 'Spam', Icon: SpamIcon, rightLabel: 13, iconColor: "#97a6ba", route: 'spam' },
    { id: 4, label: 'Trash', Icon: TrashIcon, iconColor: "#97a6ba", route: 'trash' },
]

const filterList: List[] = [
    { id: 0, label: 'Starred', Icon: StarredIcon, iconColor: '#97a6ba', route: 'starred' },
    { id: 1, label: 'Important', Icon: ImportantIcon, iconColor: '#97a6ba', route: 'important' }
]

const labelList: List[] = [
    { id: 0, label: 'Personal', Icon: LabelIcon, iconColor: "rgba(63,131,248,1)", route: 'personal' },
    { id: 1, label: 'Work', Icon: LabelIcon, iconColor: "rgba(104,117,245,1)", route: 'work' },
    { id: 2, label: 'Payments', Icon: LabelIcon, iconColor: "rgba(240,82,82,1)", route: 'payments' },
    { id: 3, label: 'Invoices', Icon: LabelIcon, iconColor: "rgba(6,148,162,1)", route: 'invoices' },
    { id: 4, label: 'Accounts', Icon: LabelIcon, iconColor: "rgba(144,97,249,1)", route: 'accounts' },
    { id: 5, label: 'Forums', Icon: LabelIcon, iconColor: "rgba(14,159,110,1)", route: 'forums' }
]

export { mailboxList, filterList, labelList };