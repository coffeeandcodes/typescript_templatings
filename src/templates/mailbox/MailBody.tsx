import React, { useContext } from 'react';
import './styles/mailbody.scss';
import MailBodyHeader from '../../components/header/MailBodyHeader';
import { ReactComponent as MailBoxIcon } from "../../assets/icons/mailbox.svg";
import { ReactComponent as ChevronSolidIcon } from "../../assets/icons/chevron-solid.svg";
import { ReactComponent as ReplyIcon } from "../../assets/icons/reply.svg";
import { ReactComponent as ReplyAllIcon } from "../../assets/icons/reply-all.svg";
import { ReactComponent as ForwardIcon } from "../../assets/icons/forward.svg";
import avatar from '../../assets/images/avatar.jpg';
import RootContext from '../../helper/context/RootContext';
import Attachments from './Attachments';

interface MailList {
    id?: number,
    from?: string,
    name?: string,
    subject?: string,
    body?: string,
    date?: string,
    isImportant?: boolean,
    hasAttachment?: boolean,
    isStarred?: boolean,
}

interface Props {
    selectedList: MailList,
    setSelectedList: React.Dispatch<React.SetStateAction<{}>>
}

interface List {
    id: number,
    name: string,
    color: string
}

const labelList: List[] = [
    { id: 0, name: 'Personal', color: "rgba(63,131,248,1)" },
    { id: 1, name: 'Work', color: "rgba(104,117,245,1)" },
    { id: 2, name: 'Payments', color: "rgba(240,82,82,1)" }
]

const MailBody: React.FC<Props> = ({ selectedList, setSelectedList }) => {
    let isMailOpen = selectedList.name ? true : false;
    const { windowWidth } = useContext(RootContext);

    function check() {
        return (windowWidth < 1200) ? isMailOpen ? 'flex' : 'none' : 'flex';
    }

    return (
        <div
            style={{ display: check() }}
            className="mailbody">

            {!selectedList.name && <div className="no-data-container">
                <MailBoxIcon className="mailbox-icon" />
                <h4>Select a mail to read</h4>
            </div>}

            {selectedList.name && <>
                <MailBodyHeader
                    selectedList={selectedList}
                    setSelectedList={setSelectedList} />
                <div className="subject-container">
                    <span className="subject">{selectedList.subject}</span>
                    <div className="label-container">
                        {labelList.map((label) => <span
                            key={label.id}
                            className="label"
                            style={{ backgroundColor: label.color }}
                        >
                            {label.name}
                        </span>)}
                    </div>
                </div>

                <div className="body-container">
                    <div className="inner-body-container">

                        <div className="sender-info">
                            <img className="avatar" src={avatar} alt="avatar" />

                            <div className="sender-info-right">
                                <p className="name">{selectedList.name}</p>
                                <div className="sender-info-right-bottom">
                                    <span className="label">to</span>
                                    <span className="label seperator">me</span>
                                    <button
                                        className="chevron-button">
                                        <ChevronSolidIcon className="icon" />
                                    </button>
                                </div>
                            </div>

                        </div>

                        <p className="body-content">{selectedList.body}</p>

                        {selectedList.hasAttachment && <Attachments />}

                        <div className="button-container">
                            <button className="button">
                                <ReplyIcon className="button-icon icon" />
                                <span className="button-text">Reply</span>
                            </button>
                            <button className="button">
                                <ReplyAllIcon className="button-icon icon" />
                                <span className="button-text">Reply All</span>
                            </button>
                            <button className="button">
                                <ForwardIcon className="button-icon icon" />
                                <span className="button-text">Forward</span>
                            </button>
                        </div>

                    </div>
                </div>
            </>}

        </div>
    );
}

export default MailBody;