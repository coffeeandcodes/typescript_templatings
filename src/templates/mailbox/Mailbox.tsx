import React, { useState } from 'react';
import "./styles/mailbox.scss";
import MailList from './MailList';
import MailBody from './MailBody';

const Mailbox: React.FC = () => {
    const [selectedList, setSelectedList] = useState({});
    return (
        <div className="mailbox">
            <MailList
                setSelectedList={setSelectedList}
                selectedList={selectedList}
            />
            <MailBody
                selectedList={selectedList}
                setSelectedList={setSelectedList}
                />
        </div>
    )
}

export default Mailbox