import React, { useContext } from 'react';
import './styles/maillist.scss';
import MailListHeader from '../../components/header/MailListHeader';
import { mailData } from "./mailData";
import RootContext from '../../helper/context/RootContext';

import { ReactComponent as StarredIcon } from "../../assets/icons/starred.svg";
import { ReactComponent as ImportantIcon } from "../../assets/icons/important.svg";
import { ReactComponent as AttachmentIcon } from "../../assets/icons/attachment.svg";
 
interface MailList {
    id?: number, 
    name?: string,
}

interface Props {
    setSelectedList: React.Dispatch<React.SetStateAction<{}>>,
    selectedList: MailList;
}

const MailList: React.FC<Props> = (props) => {
    const { setSelectedList, selectedList } = props;
    let isMailOpen = selectedList.name ? true : false;
    const { windowWidth } = useContext(RootContext);

    return (
        <div
            style={{ display: (isMailOpen && windowWidth < 1200) ? 'none' : 'flex' }}
            className="maillist-container">
            <MailListHeader />

            <div className="body-container">
                {mailData.map((data, index) => <div
                    key={index}
                    onClick={() => {
                        setSelectedList(data)
                    }}
                    className={`list-container${selectedList.id === index ? ' active-list' : ''}`}
                    style={{
                        borderTopWidth: index === 0 ? 0 : 1,
                    }}
                >
                    <div className="top-container">
                        <div className="top-right-container">
                            <span className="receiptent-name">{data.name}</span>
                            {data.isImportant && <ImportantIcon className="icon" />}
                        </div>
                        <span className="received-date">{data.date}</span>
                    </div>
                    <div className="subject-container">
                        <span className="subject">{data.subject}</span>
                        <div className="subject-right-container">
                            {data.hasAttachment && <AttachmentIcon className="icon attachment" />}
                            {data.isStarred && <StarredIcon className="icon" />}
                        </div>
                    </div>
                    <span className="body">{data.body}</span>
                </div>)}
            </div>

        </div>
    );
}

export default MailList;