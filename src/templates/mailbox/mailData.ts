interface MailList {
    id: number,
    from: string,
    name: string,
    subject: string,
    body: string,
    date: string,
    isImportant: boolean,
    hasAttachment: boolean,
    isStarred: boolean,
}

export const mailData: MailList[] = [
    {
        id: 0,
        from: 'concepcion.cleveland@company.com',
        name: 'Myra Dudley',
        subject: 'Please review and sign the attached agreement',
        body: `Hi Andrew,

        Ullamco deserunt commodo esse deserunt deserunt quis eiusmod. Laborum sint excepteur non sit eiusmod sunt voluptate ipsum nisi ullamco magna. Lorem consectetur est dolor minim exercitation deserunt quis duis fugiat ipsum incididunt non. Anim aute ipsum cupidatat nisi occaecat quis sit nisi labore labore dolore do. Pariatur veniam culpa quis veniam nisi exercitation veniam ut. Quis do sint proident fugiat ad.
        
        Non id nisi commodo veniam. Veniam veniam minim ea laborum voluptate id duis deserunt. Anim ut ut amet et ullamco nulla fugiat id incididunt adipisicing excepteur amet. Ex amet eu cillum non fugiat velit dolore. Incididunt duis est eu et ex sunt consectetur cillum nisi aute proident.
        
        Incididunt excepteur laborum quis sit. Ex quis officia incididunt proident aliqua adipisicing. Irure ad in Lorem laborum deserunt nulla consequat. Pariatur excepteur exercitation cupidatat aute.
        
        Cheers!
        Myra Dudley`,
        date: 'Aug 17',
        isImportant: true,
        hasAttachment: false,
        isStarred: false,

    },
    {
        id: 1,
        from: 'goff.jennings@company.com',
        name: 'Goff Jennings',
        subject: 'Labore sint dolor nulla nostrud commodo amet nisi mollit commodo eiusmod duis quis irure non',
        body: `Dear Andrew,

        Nisi sit ut in do aliqua nostrud consectetur incididunt. Non et pariatur nulla mollit aute aliquip amet minim irure tempor eu id ipsum. Velit sunt tempor proident voluptate ad reprehenderit. Dolor consectetur est in nulla. Reprehenderit incididunt magna deserunt mollit officia non aliqua. Elit est dolore ea Lorem velit ipsum occaecat cupidatat. Mollit magna laborum qui sit sunt mollit amet.
        
        Duis excepteur labore laboris adipisicing culpa culpa eiusmod et velit aliquip velit. Proident tempor in excepteur minim irure duis ex in non est. Labore minim sunt culpa enim tempor labore ea adipisicing nulla elit magna. Fugiat enim ex voluptate officia pariatur pariatur ipsum eu in. Veniam commodo occaecat laborum excepteur nisi Lorem.
        
        Excepteur adipisicing amet ea commodo dolor nisi labore deserunt adipisicing pariatur. Pariatur magna et esse id occaecat minim minim. Labore cupidatat tempor deserunt reprehenderit anim duis magna laborum excepteur aliquip consectetur.
        
        Best Regards,
        Goff Jennings`,
        date: 'Aug 07',
        isImportant: true,
        hasAttachment: true,
        isStarred: true,

    },
    {
        id: 2,
        from: 'brooke.petersen@company.com',
        name: 'Brooke Petersen',
        subject: 'Anim laboris aliquip excepteur consectetur eu enim sunt velit qui deserunt',
        body: `Hello Andrew,

        Consequat velit voluptate exercitation sint anim laboris. Consectetur dolor sunt veniam incididunt ad laboris proident tempor voluptate enim excepteur. Nostrud eu id tempor cupidatat. Deserunt ullamco consequat esse et. Dolore qui cupidatat commodo ea nisi tempor velit sit aliquip amet.
        
        Magna fugiat cupidatat mollit mollit. Consectetur consequat occaecat pariatur commodo quis labore est cillum voluptate culpa tempor elit incididunt. Voluptate anim est eiusmod voluptate ipsum commodo do et elit. Aute pariatur adipisicing eu laboris proident Lorem qui enim magna adipisicing deserunt pariatur. Fugiat eiusmod occaecat dolor tempor sunt exercitation est amet mollit est. Est in duis adipisicing nostrud aute voluptate quis in fugiat veniam reprehenderit.
        
        Ipsum id deserunt ex non nisi nostrud enim pariatur nulla. In labore qui esse veniam ut. Est id ut pariatur esse nulla dolore aliqua ad aliqua fugiat. Ad incididunt amet culpa labore enim proident tempor. Aliquip non dolore sunt eu deserunt tempor anim qui dolore quis. Est sunt enim ipsum aliqua.
        
        Kind Regards,
        Brooke Petersen`,
        date: 'Jul 22',
        isImportant: false,
        hasAttachment: true,
        isStarred: true,

    },
    {
        id: 3,
        from: 'estes.walter@company.com',
        name: 'Estes Walter',
        subject: 'Reprehenderit ad do quis ut fugiat proident labore',
        body: `Hello Andrew,

        Fugiat labore incididunt aute sint id laboris nisi eiusmod reprehenderit. Sint sint Lorem aute cillum velit occaecat sit quis laboris ipsum laborum. Ex ipsum ea proident duis ex nostrud dolore exercitation nostrud ullamco cupidatat irure dolor. In aliqua occaecat commodo irure dolore. Nisi laborum anim cillum aute adipisicing labore fugiat velit officia cupidatat aliquip voluptate veniam. Aute incididunt consequat est id commodo elit occaecat ea Lorem deserunt est.
        
        Pariatur deserunt sunt excepteur nisi ex. Enim consequat esse in deserunt ut. Cillum incididunt exercitation fugiat reprehenderit amet dolor nulla irure id quis.
        
        Enim id incididunt labore commodo voluptate. Non sint sint in eu anim dolor aliquip ullamco occaecat esse id consectetur cupidatat. Mollit aute nisi et fugiat consequat.
        
        Kind Regards,
        Estes Walter`,
        date: 'Jul 17',
        isImportant: true,
        hasAttachment: false,
        isStarred: true,

    },
    {
        id: 4,
        from: 'concepcion.cleveland@company.com',
        name: 'Myra Dudley',
        subject: 'Please review and sign the attached agreement',
        body: `Hi Andrew,

        Ullamco deserunt commodo esse deserunt deserunt quis eiusmod. Laborum sint excepteur non sit eiusmod sunt voluptate ipsum nisi ullamco magna. Lorem consectetur est dolor minim exercitation deserunt quis duis fugiat ipsum incididunt non. Anim aute ipsum cupidatat nisi occaecat quis sit nisi labore labore dolore do. Pariatur veniam culpa quis veniam nisi exercitation veniam ut. Quis do sint proident fugiat ad.
        
        Non id nisi commodo veniam. Veniam veniam minim ea laborum voluptate id duis deserunt. Anim ut ut amet et ullamco nulla fugiat id incididunt adipisicing excepteur amet. Ex amet eu cillum non fugiat velit dolore. Incididunt duis est eu et ex sunt consectetur cillum nisi aute proident.
        
        Incididunt excepteur laborum quis sit. Ex quis officia incididunt proident aliqua adipisicing. Irure ad in Lorem laborum deserunt nulla consequat. Pariatur excepteur exercitation cupidatat aute.
        
        Cheers!
        Myra Dudley`,
        date: 'Aug 17',
        isImportant: false,
        hasAttachment: false,
        isStarred: false,

    },
    {
        id: 5,
        from: 'goff.jennings@company.com',
        name: 'Goff Jennings',
        subject: 'Labore sint dolor nulla nostrud commodo amet nisi mollit commodo eiusmod duis quis irure non',
        body: `Dear Andrew,

        Nisi sit ut in do aliqua nostrud consectetur incididunt. Non et pariatur nulla mollit aute aliquip amet minim irure tempor eu id ipsum. Velit sunt tempor proident voluptate ad reprehenderit. Dolor consectetur est in nulla. Reprehenderit incididunt magna deserunt mollit officia non aliqua. Elit est dolore ea Lorem velit ipsum occaecat cupidatat. Mollit magna laborum qui sit sunt mollit amet.
        
        Duis excepteur labore laboris adipisicing culpa culpa eiusmod et velit aliquip velit. Proident tempor in excepteur minim irure duis ex in non est. Labore minim sunt culpa enim tempor labore ea adipisicing nulla elit magna. Fugiat enim ex voluptate officia pariatur pariatur ipsum eu in. Veniam commodo occaecat laborum excepteur nisi Lorem.
        
        Excepteur adipisicing amet ea commodo dolor nisi labore deserunt adipisicing pariatur. Pariatur magna et esse id occaecat minim minim. Labore cupidatat tempor deserunt reprehenderit anim duis magna laborum excepteur aliquip consectetur.
        
        Best Regards,
        Goff Jennings`,
        date: 'Aug 07',
        isImportant: true,
        hasAttachment: true,
        isStarred: false,

    },
    {
        id: 6,
        from: 'brooke.petersen@company.com',
        name: 'Brooke Petersen',
        subject: 'Anim laboris aliquip excepteur consectetur eu enim sunt velit qui deserunt',
        body: `Hello Andrew,

        Consequat velit voluptate exercitation sint anim laboris. Consectetur dolor sunt veniam incididunt ad laboris proident tempor voluptate enim excepteur. Nostrud eu id tempor cupidatat. Deserunt ullamco consequat esse et. Dolore qui cupidatat commodo ea nisi tempor velit sit aliquip amet.
        
        Magna fugiat cupidatat mollit mollit. Consectetur consequat occaecat pariatur commodo quis labore est cillum voluptate culpa tempor elit incididunt. Voluptate anim est eiusmod voluptate ipsum commodo do et elit. Aute pariatur adipisicing eu laboris proident Lorem qui enim magna adipisicing deserunt pariatur. Fugiat eiusmod occaecat dolor tempor sunt exercitation est amet mollit est. Est in duis adipisicing nostrud aute voluptate quis in fugiat veniam reprehenderit.
        
        Ipsum id deserunt ex non nisi nostrud enim pariatur nulla. In labore qui esse veniam ut. Est id ut pariatur esse nulla dolore aliqua ad aliqua fugiat. Ad incididunt amet culpa labore enim proident tempor. Aliquip non dolore sunt eu deserunt tempor anim qui dolore quis. Est sunt enim ipsum aliqua.
        
        Kind Regards,
        Brooke Petersen`,
        date: 'Jul 22',
        isImportant: true,
        hasAttachment: false,
        isStarred: true,

    },
    {
        id: 7,
        from: 'estes.walter@company.com',
        name: 'Estes Walter',
        subject: 'Reprehenderit ad do quis ut fugiat proident labore',
        body: `Hello Andrew,

        Fugiat labore incididunt aute sint id laboris nisi eiusmod reprehenderit. Sint sint Lorem aute cillum velit occaecat sit quis laboris ipsum laborum. Ex ipsum ea proident duis ex nostrud dolore exercitation nostrud ullamco cupidatat irure dolor. In aliqua occaecat commodo irure dolore. Nisi laborum anim cillum aute adipisicing labore fugiat velit officia cupidatat aliquip voluptate veniam. Aute incididunt consequat est id commodo elit occaecat ea Lorem deserunt est.
        
        Pariatur deserunt sunt excepteur nisi ex. Enim consequat esse in deserunt ut. Cillum incididunt exercitation fugiat reprehenderit amet dolor nulla irure id quis.
        
        Enim id incididunt labore commodo voluptate. Non sint sint in eu anim dolor aliquip ullamco occaecat esse id consectetur cupidatat. Mollit aute nisi et fugiat consequat.
        
        Kind Regards,
        Estes Walter`,
        date: 'Jul 17',
        isImportant: true,
        hasAttachment: true,
        isStarred: true,

    }
]