import React from 'react';
import "./styles/attachment.scss";
import { ReactComponent as AttachmentIcon } from "../../assets/icons/attachment.svg";
import image1 from "../../assets/images/birds-eye-sydney_preview.jpg";
import image2 from "../../assets/images/mystery-forest_preview.jpg";

interface Props {

}

const Attachments: React.FC<Props> = () => {
    return (
        <>
            <div className="attachment-header">
                <AttachmentIcon className="icon" />
                <span className="attachment-text">3 Attachments</span>
            </div>

            <div className="attachment-container">
                <div className="attachment">
                    <img className="attachment-image" src={image1} alt="attachment" />
                    <div className="attachment-right">
                        <span className="file-text">mystery-forest.jpg</span>
                        <span className="file-text size">15.54 KB</span>
                    </div>
                </div>

                <div className="attachment">
                    <div className="pdf-container">
                        <span className="pdf-text">PDF</span>
                    </div>
                    <div className="attachment-right">
                        <span className="file-text">monthly-income.pdf</span>
                        <span className="file-text size">243.45 KB</span>
                    </div>
                </div>

                <div className="attachment">
                    <img className="attachment-image" src={image2} alt="attachment" />
                    <div className="attachment-right">
                        <span className="file-text">birds-eye-sydney.jpg</span>
                        <span className="file-text size">14.29 KB</span>
                    </div>
                </div>
            </div>
        </>
    );
}

export default Attachments;