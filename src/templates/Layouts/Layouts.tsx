import React from 'react';
import "./style/Layout.scss";
import LayoutFirstBlock from './LayoutFirstBlock';
import LayoutSecondBlock from './LayoutSecondBlock';
import LayoutThirdBlock from './LayoutThirdBlock';
import LayoutFourthBlock from './LayoutFourthBlock';
import LayoutFifthBlock from './LayoutFifthBlock';
import LayoutSixthBlock from './LayoutSixthBlock';


interface Props {

}

const Layout = (props: Props) => {

    return (
        <div className="form-container">
            <h1 className="form-main-title">UI Elements</h1>

            <LayoutFirstBlock />

            <LayoutSecondBlock />

            <LayoutThirdBlock/>

            <LayoutFourthBlock/>

            <LayoutFifthBlock/>

            <LayoutSixthBlock/>

        </div>
    )
}

export default Layout;