import React, { useState } from 'react';
import TextBox from '../../components/forms/TextBox';
import "./style/LayoutSecondBlock.scss";
import { ReactComponent as BackArrowIcon } from "../../assets/icons/back-arrow.svg";

import { Switch, CheckBox, RadioButton, ChevronButton } from '../../components/forms';


import Select, { ValueType } from 'react-select';
import Slider from './Slider';

type OptionType = {
    value: string;
    label: string;
};

const options: OptionType[] = [
    { value: '1', label: 'Select 1' },
    { value: '2', label: 'Select 2' },
    { value: '3', label: 'Select 3' },
];

interface Props {

}

let customStyles = {
    control: (base: any) => ({
        ...base,
        background: "#F5F5F5",
    }),
}

const LayoutSecondBlock = (props: Props) => {
    const [switch1, setSwitch1] = useState(false);
    const [switch2, setSwitch2] = useState(true);
    const [selectedOption1, setSelectedOption1] = useState<ValueType<OptionType>>(null);
    const [sliderValue, setSliderValue] = useState(40);
    return (
        <div className="second-block">
            <div className="second-block-left">
                <TextBox defaultValue="Binod Shrestha" label="Name" />
                <TextBox label="E-mail" />
                <TextBox label="Password" type="Password" />

                <div style={{ display: 'flex' }}>
                    <Switch containerStyle={{ marginRight: 20 }} onChange={(value) => setSwitch1(value)} value={switch1} />
                    <Switch onChange={(value) => setSwitch2(value)} value={switch2} />
                </div>

                <div style={{ display: 'flex', marginTop: 30 }}>
                    <ChevronButton style={{ backgroundColor: '#F5F5F5', borderColor: '#E4E4E4', marginRight: 20 }} customicon={BackArrowIcon} />
                    <ChevronButton style={{ backgroundColor: '#F5F5F5', borderColor: '#E4E4E4', marginRight: 20 }} direction="right" customicon={BackArrowIcon} />
                    <ChevronButton size={15} style={{ backgroundColor: '#F5F5F5', borderColor: '#E4E4E4', marginRight: 20 }} />
                    <ChevronButton size={15} style={{ backgroundColor: '#F5F5F5', borderColor: '#E4E4E4' }} />
                </div>

                <div style={{ display: 'flex', marginTop: 30 }}>
                    <CheckBox containerStyle={{ marginRight: 20 }} size={15} borderColor="#E4E4E4" activeBackGround="#5876F0" tickColor="white" />
                    <CheckBox containerStyle={{ marginRight: 20 }} size={15} borderColor="#E4E4E4" activeBackGround="#5876F0" tickColor="white" />
                    <CheckBox containerStyle={{ marginRight: 20 }} size={15} borderColor="#E4E4E4" activeBackGround="#5876F0" tickColor="white" value={true} />
                </div>
                <div style={{ display: 'flex', marginTop: 30 }}>
                    <RadioButton containerStyle={{ marginRight: 20 }} size={15} borderColor="#E4E4E4" activeBackGround="#5876F0" activeColor="white" />
                    <RadioButton containerStyle={{ marginRight: 20 }} size={15} borderColor="#E4E4E4" activeBackGround="#5876F0" activeColor="white" />
                    <RadioButton containerStyle={{ marginRight: 20 }} size={15} borderColor="#E4E4E4" activeBackGround="#5876F0" activeColor="white" value={true} />
                </div>
            </div>

            <div className="second-block-right">
                <p className="label-title">Dropdown</p>
                <Select
                    value={selectedOption1}
                    onChange={(value: ValueType<OptionType>) => setSelectedOption1(value)}
                    options={options}
                    placeholder="Select"
                    styles={customStyles}
                    className="picker"
                />

                <Slider
                    value={sliderValue}
                    onChange={({ target }) => setSliderValue(+target.value)}
                    borderVisible={false}
                    thumbVisible={false}
                />
            </div>
        </div>
    )
}

export default LayoutSecondBlock;