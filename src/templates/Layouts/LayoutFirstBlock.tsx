import React, { useState } from 'react';
import "./style/LayoutFirstBlock.scss";

import { CheckBox, RadioButton, Button, ChevronButton } from '../../components/forms';


interface Props {

}

const LayoutFirstBlock = (props: Props) => {
    const [radio1, setRadio1] = useState(false);
    const [radio2, setRadio2] = useState(true);
    const [checkbox1, setCheckbox1] = useState(false);
    const [checkbox2, setCheckbox2] = useState(true);
    return (
        <div className="button-group">
            <span className="button-main-title">Buttons</span>
            <div className="button-group-inner">
                <div className="button-group-left">
                    <div>
                        <Button title="Normal" backgroundcolor="#43425D" />
                        <Button title="Pressed" backgroundcolor="#2F2E50" />
                        <Button title="Disabled" backgroundcolor="#43425D" disabled={true} />
                    </div>
                    <div>
                        <Button title="Normal" color="#2F2E50" />
                        <Button title="Pressed" color="#2F2E50" />
                        <Button title="Disabled" color="#4D4F5C" disabled={true} />
                    </div>

                    <div style={{ marginTop: 30 }}>
                        <Button title="Normal" backgroundcolor="#3B86FF" />
                        <Button title="Pressed" backgroundcolor="#1062E5" />
                        <Button title="Disabled" backgroundcolor="#1062E5" disabled={true} />
                    </div>
                    <div>
                        <Button title="Normal" color="#3B86FF" bordercolor="#3B86FF" />
                        <Button title="Pressed" color="#1062E5" bordercolor="#1062E5" />
                        <Button title="Disabled" color="#1062E5" bordercolor="#1062E5" disabled={true} />
                    </div>
                </div>

                <div className="button-group-right">
                    <div style={{ display: 'flex' }}>
                        <ChevronButton style={{ marginRight: 20 }} />
                        <ChevronButton direction="right" />
                    </div>

                    <div className="radio-checkbox-group">
                        <div className="radio-button-group">
                            <RadioButton containerStyle={{ marginRight: 20 }} disabled={true} />
                            <RadioButton containerStyle={{ marginRight: 20 }} onChange={(value) => setRadio1(value)} value={radio1} />
                            <RadioButton onChange={(value) => setRadio2(value)} value={radio2} />
                        </div>

                        <div className="checkbox-button-group">
                            <CheckBox containerStyle={{ marginRight: 20 }} disabled={true} />
                            <CheckBox containerStyle={{ marginRight: 20 }} value={checkbox1} onChange={(value) => setCheckbox1(value)} />
                            <CheckBox value={checkbox2} onChange={(value) => setCheckbox2(value)} />
                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}

export default LayoutFirstBlock;