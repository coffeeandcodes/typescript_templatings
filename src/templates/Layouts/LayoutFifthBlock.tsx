import React, { useState } from 'react';
import './style/LayoutFifthBlock.scss';
import Slider from './Slider';
import { ReactComponent as MenuIcon } from "../../assets/icons/menu.svg";
import { ReactComponent as HambrugerIcon } from "../../assets/icons/hamburger.svg";
import { ReactComponent as GridIcon } from "../../assets/icons/grid.svg";
import { ReactComponent as BackArrowIcon } from "../../assets/icons/back-arrow.svg";

import { Switch, CheckBox, RadioButton, TextArea, ChevronButton } from '../../components/forms';



interface Props {

}

let chevronButtonStyle = { backgroundColor: '#FFFFFF', borderWidth: 0, height: 40, width: 40 }

let tagsList = ['JavaScript', 'Reaact', 'React Native', "UI", 'Adobe', 'Graphics', 'Icon', 'Web design', 'Web', "Design"]

const LayoutFifthBlock = (props: Props) => {
    const [switch1, setSwitch1] = useState(false);
    const [switch2, setSwitch2] = useState(true);
    const [switch3, setSwitch3] = useState(false);
    const [switch4, setSwitch4] = useState(true);
    const [activeNum, setActiveNum] = useState(1);
    return (
        <div className="fifth-block">
            <div className="fifth-block-left">
                <div style={{ display: 'flex', flexDirection: 'column' }}>
                    <Slider thumbVisible={false} borderVisible={false} cotainerStyle={{ width: 360, marginTop: 0 }} />
                    <Slider thumbVisible={false} borderVisible={false} cotainerStyle={{ width: 360 }} />
                    <Slider borderVisible={false} cotainerStyle={{ width: 360 }} />
                </div>

                <div className="menu-icon-container">
                    {[HambrugerIcon, MenuIcon, GridIcon].map((Icon, index) => <button
                        key={index}
                        style={{ borderRightWidth: index !== 2 ? 1 : 0 }}
                        className="icon-button">
                        <Icon width={16} height={16} fill="white" />
                    </button>)}
                </div>

                <div style={{ display: 'flex', marginTop: 30 }}>
                    <Switch
                        mode="text"
                        switchButtonStyle={{ borderRadius: 40 }}
                        containerStyle={{ borderRadius: 40, marginRight: 10 }}
                        onChange={(value) => setSwitch1(value)}
                        value={switch1} />

                    <Switch
                        mode="text"
                        switchButtonStyle={{ borderRadius: 40 }}
                        containerStyle={{ borderRadius: 40, marginRight: 10 }}
                        onChange={(value) => setSwitch2(value)}
                        value={switch2} />

                    <Switch
                        mode="icon"
                        containerStyle={{ marginRight: 10 }}
                        onChange={(value) => setSwitch3(value)}
                        value={switch3} />
                    <Switch
                        mode="icon"
                        onChange={(value) => setSwitch4(value)}
                        value={switch4} />
                </div>

                <div style={{ display: 'flex', marginTop: 50, alignItems: 'center', alignSelf: 'flex-start' }}>
                    <ChevronButton
                        onClick={() => activeNum !== 1 ? setActiveNum(+activeNum - 1) : null}
                        style={chevronButtonStyle}
                        customicon={BackArrowIcon} />
                    {[1, 2, 3, 4, 5].map(num => <p
                        style={{ color: activeNum === num ? '#585A5D' : '#E4E4E4' }}
                        className="num-text"
                        key={num}>{num}</p>)}
                    <ChevronButton
                        onClick={() => activeNum !== 5 ? setActiveNum(+activeNum + 1) : null}
                        direction="right"
                        style={chevronButtonStyle}
                        customicon={BackArrowIcon} />
                </div>

                <div style={{ display: 'flex', marginTop: 40 }}>
                    <CheckBox borderColor="#E4E4E4" containerStyle={{ marginRight: 20 }} />
                    <CheckBox borderColor="#E4E4E4" containerStyle={{ marginRight: 20 }} />
                    <CheckBox borderColor="#E4E4E4" value={true} containerStyle={{ marginRight: 40 }} />

                    <RadioButton borderColor="#E4E4E4" containerStyle={{ marginRight: 20 }} />
                    <RadioButton borderColor="#E4E4E4" containerStyle={{ marginRight: 20 }} />
                    <RadioButton borderColor="#E4E4E4" value={true} />
                </div>
            </div>

            <div className="fifth-block-right">
                <p className="tag-title">Tags</p>
                <div className="tags-container">
                    {tagsList.map((tag, index) => <p className="tags-list" key={index}>{tag}</p>)}
                </div>

                <TextArea
                    containerstyle={{ borderRadius: 4, backgroundColor: 'white', marginTop: 30 }}
                    placeholder="Type something…"
                    borderColor="#E4E4E4"
                />

                <TextArea
                    containerstyle={{ borderRadius: 4, backgroundColor: 'white' }}
                    placeholder="Type something…"
                    borderColor="#E4E4E4"
                    defaultValue="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut ero labore et dolore"
                />
            </div>

        </div>
    )
}

export default LayoutFifthBlock
