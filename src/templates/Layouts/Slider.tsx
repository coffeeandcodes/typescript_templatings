import React from 'react'
import './style/Slider.scss';

interface Props {
    min: number,
    max: number,
    value: number,
    activeBackground: string,
    inactiveBackground: string,
    borderVisible?: boolean,
    thumbVisible: boolean,
    cotainerStyle?: React.CSSProperties
}

const Slider = (props: Props & React.InputHTMLAttributes<HTMLInputElement>) => {
    const { max, value, activeBackground, inactiveBackground, borderVisible, thumbVisible, cotainerStyle } = props;
    let background = `linear-gradient(to right, ${activeBackground} ${value / max * 100}% ${value / max * 100}%, 
        ${inactiveBackground} ${100 - (max - value) / max * 100}%)`
    return (
        <input
            className={`custom-slider ${thumbVisible ? 'custom-slider-thumbvisible' : ''}`}
            style={{ background, borderWidth: borderVisible ? 2 : 0, ...cotainerStyle }}
            type="range" {...props} />
    )
}

Slider.defaultProps = {
    min: 0,
    max: 100,
    value: 40,
    activeBackground: '#5876F0',
    inactiveBackground: '#F5F5F5',
    borderVisible: true,
    thumbVisible: true
}

export default Slider;