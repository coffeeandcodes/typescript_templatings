import React, { ReactElement, useState } from 'react';
import './style/LayoutThirdBlock.scss';

import Select, { ValueType } from 'react-select';
import { Button } from '../../components/forms';

type OptionType = {
    value: string;
    label: string;
};

const options: OptionType[] = [
    { value: '1', label: 'Select 1' },
    { value: '2', label: 'Select 2' },
    { value: '3', label: 'Select 3' },
];

interface Props {

}

let customStyles = {
    control: (base: any) => ({
        ...base,
        borderRadius: 0,
        borderWidth: 0,
        borderBottomWidth: 2,
        borderColor: '#E4E4E4',
        '&:hover': {
            borderColor: '#E4E4E4'
        },
        boxShadow: 'none',
        outline: 'none',
    }),
}


const ListBlock = () => {
    return (
        <div>
            <List />
            <List />
            <div className="list-footer">
                <h3 className="primary-text">Total</h3>
                <h3 className="primary-text">$ 2,100</h3>
            </div>
        </div>
    )
}

const List = () => {
    const [select1, setSelect2] = useState<ValueType<OptionType>>(null);
    return (
        <div className="item-list">
            <div>
                <h2 className="primary-text">Item Full Name</h2>
                <p className="list-subTitle">Description of item</p>
            </div>

            <Select
                value={select1}
                onChange={(value: ValueType<OptionType>) => setSelect2(value)}
                options={options}
                placeholder="Choose licence"
                styles={customStyles}
                components={{
                    IndicatorSeparator: () => null
                }}
                className="select-picker"
            />
            <Select
                value={select1}
                onChange={(value: ValueType<OptionType>) => setSelect2(value)}
                options={options}
                placeholder="Users"
                styles={customStyles}
                components={{
                    IndicatorSeparator: () => null
                }}
                className="select-picker small"
            />

            <h3 className="primary-text">$ 1,500</h3>

        </div>
    )
}


interface Props {

}

let numofBlock = [
    { id: 0, block: <ListBlock /> },
    { id: 1, block: <ListBlock /> },
    { id: 2, block: <ListBlock /> },
]

function LayoutThirdBlock(props: Props): ReactElement {
    const [activeBlock, setActiveBlock] = useState(0);
    return (
        <div className="third-block">
            <div className="inner-third-block">
                <div className="indicator-outer-container">
                    <div
                        style={{ transform: `translateX(${(130 * activeBlock + 26 + 6 * (activeBlock))}px)` }}
                        className="outer-circle" />
                    {numofBlock.map(block => <div
                        className="indicator-container"
                        key={block.id}>
                        <div className="circle" />
                        {block.id !== 2 && <div className="line" />}
                    </div>)}
                </div>

                {numofBlock[activeBlock].block}

                <Button
                    onClick={() => activeBlock !== 2 ? setActiveBlock(activeBlock + 1) : null}
                    containerstyle={{ marginTop: 40 }}
                    color="#585A5D"
                    bordercolor="#E4E4E4"
                    title={activeBlock !== 2 ? 'Next' : 'Finish'} />
            </div>

        </div>
    )
}

export default LayoutThirdBlock;