import React, { ReactElement, useEffect, useState } from 'react'
import './style/LayoutFourthBlock.scss';
import { useForm } from "react-hook-form";
import { ReactComponent as ChevronRight } from "../../assets/icons/chevron-right.svg";

import Select, { ValueType } from 'react-select';
import { CheckBox, RadioButton, TextArea, TextBox } from '../../components/forms';

type OptionType = {
    value: string;
    label: string;
};

const options: OptionType[] = [
    { value: '1', label: 'Select 1' },
    { value: '2', label: 'Select 2' },
    { value: '3', label: 'Select 3' },
];

interface Props {

}

let customStyles = {
    control: (base: any) => ({
        ...base,
        borderColor: '#E4E4E4',
        '&:hover': {
            borderColor: '#E4E4E4'
        },
        boxShadow: 'none',
        outline: 'none',
        backgroundColor: 'white',
        marginBottom: 20,
        width: 360
    }),
}

interface Props {

}

const textBoxProps = {
    containerstyle: { borderRadius: 0, width: 360, backgroundColor: 'white' },
    borderColor: '#E4E4E4'
}

const textboxMiniProps = {
    containerstyle: {
        borderRadius: 0,
        width: 100,
        backgroundColor: 'white'
    },
    placeholder: '0.0',
    borderColor: '#E4E4E4'
}


function LayoutFourthBlock(props : Props): ReactElement {
    const { register, errors, setError } = useForm({
        mode: "all",
        reValidateMode: 'onChange',
    });
    const [select1, setSelect1] = useState<ValueType<OptionType>>(null);
    const [select2, setSelect2] = useState<ValueType<OptionType>>(options[0]);

    useEffect(() => {
        setError("field4", {
            type: "manual",
            message: "Invalid email address"
        });
    }, [])

    return (
        <div className="forth-block">
            <div>
                <TextBox
                    {...textBoxProps}
                    placeholder="Required Field"
                    name="field1"
                    ref={register({
                        required: true,
                    })}
                    error={errors['field1']}
                />

                <TextBox
                    {...textBoxProps}
                    placeholder="Focused"
                    name="field2"
                    onChange={() => {
                        setError("field2", {
                            type: "manual",
                            message: "Dont Forget Your Username Should Be Cool!"
                        });
                    }}
                    ref={register}
                    error={errors['field2']}
                />

                <TextBox
                    {...textBoxProps}
                    placeholder="Entered"
                    name="field3"
                    ref={register}
                    defaultValue="Entered"
                />

                <TextBox
                    {...textBoxProps}
                    placeholder="Entered"
                    name="field3"
                    ref={register}
                    defaultValue="Entered"
                />

                <TextBox
                    {...textBoxProps}
                    name="field4"
                    ref={register({
                        required: true,
                        pattern: {
                            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                            message: "Invalid email address"
                        }
                    })}
                    defaultValue="error@mail"
                    error={errors['field4']}
                    placeholder="Error"
                />

                <Select
                    value={select1}
                    onChange={(value: ValueType<OptionType>) => setSelect1(value)}
                    options={options}
                    placeholder="Select an option"
                    styles={customStyles}
                    components={{
                        IndicatorSeparator: () => null
                    }}
                />

                <Select
                    value={select2}
                    onChange={(value: ValueType<OptionType>) => setSelect2(value)}
                    options={options}
                    placeholder="Select an option"
                    styles={customStyles}
                    components={{
                        IndicatorSeparator: () => null
                    }}
                />
            </div>

            <div>
                <TextBox {...textboxMiniProps} />
                <TextBox {...textboxMiniProps} />
                <TextBox
                    {...textboxMiniProps}
                    defaultValue={5.8}
                />
                <TextBox
                    {...textboxMiniProps}
                    defaultValue={5.8}
                />
                <TextBox
                    {...textboxMiniProps}
                    defaultValue={5.8}
                />

                <TextBox
                    {...textboxMiniProps}
                    rightInner={<RightInner />}
                />

                <TextBox
                    {...textboxMiniProps}
                    defaultValue={5.8}
                    rightInner={<RightInner />}
                />
            </div>

            <div>
                <div style={{ display: 'flex' }}>
                    <div>
                        <CheckBox
                            label="Select"
                            borderColor="#E4E4E4"
                            tickColor="#585A5D"
                            containerStyle={{ marginBottom: 40 }}
                        />
                        <CheckBox
                            label="Focused"
                            borderColor="#E4E4E4"
                            tickColor="#585A5D"
                            containerStyle={{ marginBottom: 40 }}
                        />
                        <CheckBox
                            label="Selected"
                            value={true}
                            borderColor="#E4E4E4"
                            tickColor="#585A5D"
                            containerStyle={{ marginBottom: 40 }}
                        />
                    </div>
                    <div style={{ marginLeft: 50 }}>
                        <RadioButton
                            borderColor="#E4E4E4"
                            label="Select"
                            containerStyle={{ marginBottom: 40 }}

                        />
                        <RadioButton
                            borderColor="#E4E4E4"
                            label="Focused"
                            containerStyle={{ marginBottom: 40 }}

                        />
                        <RadioButton
                            borderColor="#E4E4E4"
                            activeColor="#585A5D"
                            value={true}
                            label="Selected"
                            containerStyle={{ marginBottom: 40 }}

                        />
                    </div>
                </div>

                <TextArea
                    containerstyle={{ borderRadius: 0, backgroundColor: 'white' }}
                    borderColor='#E4E4E4'
                />

                <TextArea
                    containerstyle={{ borderRadius: 0, backgroundColor: 'white' }}
                    borderColor='#E4E4E4'
                    defaultValue="Lorem ipsum dolor sit amet, consectetur adipisicing elit"
                />
            </div>

        </div>
    )
}

export default LayoutFourthBlock;


const RightInner = () => (
    <div>
        <button className="chevron-button">
            <ChevronRight style={{ transform: 'rotate(90deg)' }} fill="#585A5D" width={10} height={10} />
        </button>
        <button className="chevron-button">
            <ChevronRight style={{ transform: 'rotate(270deg)' }} fill="#585A5D" width={10} height={10} />
        </button>
    </div>
)