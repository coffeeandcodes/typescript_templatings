import React, { useState } from 'react';
import './style/LayoutSixthBlock.scss';
import Select, { ValueType } from 'react-select';
import { ReactComponent as SearchIcon } from "../../assets/icons/search.svg";
import { ReactComponent as HeartIcon } from "../../assets/icons/heart.svg";

import { Switch, CheckBox, RadioButton, TextArea, Button, TextBox } from '../../components/forms';


type OptionType = {
    value: string;
    label: string;
};

const options: OptionType[] = [
    { value: '1', label: 'Select 1' },
    { value: '2', label: 'Select 2' },
    { value: '3', label: 'Select 3' },
];

let customStyles = {
    control: (base: any) => ({
        ...base,
        '&:hover': {
            borderColor: '#E4E4E4'
        },
        boxShadow: 'none',
        outline: 'none',
        borderWidth: 0,
        backgroundColor: 'white',
        marginBottom: 20,
        width: 360,
    }),
    dropdownIndicator: (defaultStyles: any) => ({
        ...defaultStyles,
        color: 'white',
        backgroundColor: '#5876F0',
        height: '100%',
        '&:hover': {
            color: 'white'
        }
    })
}

interface Props {

}

const LayoutSixthBlock = (props: Props) => {
    const [select1, setSelect1] = useState<ValueType<OptionType>>(null);
    const [textAreaValue, setTextAreaValue] = useState('');
    const [switch1, setSwitch1] = useState(false);
    const [switch2, setSwitch2] = useState(true);

    const [checkbox1, setCheckbox1] = useState(false);
    const [checkbox2, setCheckbox2] = useState(false);
    const [checkbox3, setCheckbox3] = useState(true);

    const [radio1, setradio1] = useState(false);
    const [radio2, setradio2] = useState(false);
    const [radio3, setradio3] = useState(true);

    return (
        <div className="sixth-block">
            <div style={{ margin: "40px 0" }}>
                <Select
                    value={select1}
                    onChange={(value: ValueType<OptionType>) => setSelect1(value)}
                    options={options}
                    placeholder="Select an option"
                    styles={customStyles}
                    components={{
                        IndicatorSeparator: () => null
                    }}
                />

                <div style={{ display: 'flex', width: 360 }}>
                    <TextBox
                        placeholder="Search"
                        containerstyle={{ borderRadius: 0, borderWidth: 0, backgroundColor: 'white' }}
                        wrapperStyle={{ flex: 1, marginBottom: 0 }} />
                    <button className="search-button">
                        <SearchIcon fill="white" height={18} width={18} />
                    </button>
                </div>

                <div style={{ marginTop: 40, display: 'flex' }}>
                    <Switch
                        value={switch1}
                        onChange={value => setSwitch1(value)}
                        switchButtonStyle={{ borderRadius: 0 }}
                        containerStyle={{ marginRight: 20, padding: 0, borderRadius: 0 }} mode="icon" />
                    <Switch
                        value={switch2}
                        onChange={value => setSwitch2(value)}
                        containerStyle={{ padding: 0, borderRadius: 0, marginRight: 20 }}
                        switchButtonStyle={{ borderRadius: 0 }}
                        mode="icon" />

                    <div
                        style={{ marginRight: 20 }}
                        className="tooltip-container">
                        <div className="triangle"></div>
                        <div
                            style={{ padding: '13px 20px' }}
                            className="tooltip">
                            <p className="tooltip-text">Like</p>
                        </div>
                    </div>

                    <div className="tooltip-container">
                        <div className="triangle"></div>
                        <div
                            className="tooltip">
                            <HeartIcon width={16} height={14} fill="white" />
                        </div>
                    </div>

                </div>

                <div style={{ display: 'flex', margin: "40px 0" }}>
                    <CheckBox activeBackGround="#5876F0"
                        inactiveBackground="white"
                        tickColor="white"
                        value={checkbox1}
                        onChange={value => setCheckbox1(value)}
                        innerContainerStyle={{ borderWidth: 0 }}
                        containerStyle={{ marginRight: 20 }} />
                    <CheckBox activeBackGround="#5876F0"
                        inactiveBackground="white"
                        tickColor="white"
                        value={checkbox2}
                        onChange={value => setCheckbox2(value)}
                        innerContainerStyle={{ borderWidth: 0 }}
                        containerStyle={{ marginRight: 20 }} />
                    <CheckBox activeBackGround="#5876F0"
                        inactiveBackground="white"
                        value={checkbox3}
                        onChange={value => setCheckbox3(value)}
                        innerContainerStyle={{ borderWidth: 0 }}
                        tickColor="white"
                        containerStyle={{ marginRight: 40 }} />


                    <RadioButton activeBackGround="#5876F0"
                        inactiveBackground="white"
                        value={radio1}
                        onChange={value => setradio1(value)}
                        innerContainerStyle={{ borderWidth: 0 }}
                        containerStyle={{ marginRight: 20 }}
                        activeColor="white"
                    />
                    <RadioButton activeBackGround="#5876F0"
                        inactiveBackground="white"
                        value={radio2}
                        onChange={value => setradio2(value)}
                        innerContainerStyle={{ borderWidth: 0 }}
                        containerStyle={{ marginRight: 20 }}
                        activeColor="white"
                    />
                    <RadioButton activeBackGround="#5876F0"
                        inactiveBackground="white"
                        value={radio3}
                        onChange={value => setradio3(value)}
                        innerContainerStyle={{ borderWidth: 0 }}
                        activeColor="white"
                    />
                </div>


            </div>

            <div style={{ display: 'flex', flexDirection: 'column', marginTop: 40 }}>
                <TextArea
                    rows={5}
                    value={textAreaValue}
                    onChange={({ target }) => setTextAreaValue(target.value)}
                    placeholder="Type something…"
                    outerContainerStyle={{ marginBottom: 0, width: 300 }}
                    containerstyle={{ borderWidth: 0, borderRadius: 0, backgroundColor: 'white' }}
                />
                <Button
                    disabled={textAreaValue ? false : true}
                    backgroundcolor="#5876F0"
                    containerstyle={{ fontSize: 10, width: 110, height: 40, borderRadius: 0, margin: 0, alignSelf: 'flex-end' }}
                    color="white"
                    title="ADD COMMENT" />

                <TextArea
                    rows={5}
                    outerContainerStyle={{ marginBottom: 0, marginTop: 50, width: 300 }}
                    containerstyle={{ borderWidth: 0, borderRadius: 0, backgroundColor: 'white' }}
                    placeholder="Type something…"
                    defaultValue="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut ero labore et dolore"
                />
                <Button
                    backgroundcolor="#5876F0"
                    containerstyle={{ fontSize: 10, width: 110, height: 40, borderRadius: 0, alignSelf: 'flex-end', margin: 0 }}
                    color="white"
                    title="ADD COMMENT" />
            </div>
        </div>
    )
}

export default LayoutSixthBlock;