import React, { useEffect } from 'react';
import "./style/Statement.scss";
import ApexCharts from 'apexcharts';
import { ReactComponent as MoreIcon } from "../../assets/icons/more.svg";
import { options } from "./chartOptions";

const statementList = [
    {
        title: 'PREVIOUS STATEMENT', subTitle: 'Paid on June 28, 2020', color: 'rgba(14,159,110,1)', amounts: [
            { label: 'Card Limit', money: "$34,500.00" },
            { label: 'SPENT', money: "$27,221.21" },
            { label: 'MINIMUM', money: "$7,331.94" }
        ]
    },
    {
        title: 'CURRENT STATEMENT', subTitle: 'Must be paid before July 28, 2020', color: 'rgba(240,82,82, 1)', amounts: [
            { label: 'Limit', money: "$34,500.00" },
            { label: 'SPENT', money: "$39,221.21" },
            { label: 'MINIMUM', money: "$9,331.94" }
        ]
    },
]


const Statement: React.FC = () => {

    useEffect(() => {
        var chart = new ApexCharts(
            document.querySelector("#chart"),
            options
        );

        chart.render();

    }, [])

    return (
        <div className="statement-container">

            <div className="statement-left">
                {statementList.map((stat, i) => <div
                    key={i}
                    className="statement"
                    style={{ borderColor: stat.color }}
                >
                    <div className="statement-top">
                        <div>
                            <p className="primary-text">{stat.title}</p>
                            <p style={{ color: stat.color }} className="secondary-text">{stat.subTitle}</p>
                        </div>
                        <button className="icon-button">
                            <MoreIcon className="icon" />
                        </button>
                    </div>
                    <div className="statement-bottom">
                        {stat.amounts.map((amt, i) => <div
                            key={i}
                            className="amount-container"
                        >
                            <p className="amount-label">{amt.label}</p>
                            <p className="amount">{amt.money}</p>
                        </div>)}
                    </div>
                </div>)}
            </div>

            <div className="statement-right">
                <div className="account">
                    <div className="account-top">
                        <div>
                            <p className="primary-text">ACCOUNT BALANCE</p>
                            <p className="secondary-text">Monthly balance growth and avg. monthly income</p>
                        </div>
                        <p className="secondary-text">12 months</p>
                    </div>

                    <div className="account-bottom">
                        <div>
                            <p className="amount">38.33%</p>
                            <p className="secondary-text">Average Monthly Growth</p>
                        </div>
                        <div className="amount-bottom-right">
                            <p className="amount">$45,332.00</p>
                            <p className="secondary-text">Average Monthly Income</p>
                        </div>
                    </div>
                </div>

                <div className="chart-container">
                    <div id="chart" />
                </div>

            </div>

        </div>

    )
}

export default Statement;