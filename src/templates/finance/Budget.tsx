import React from 'react';
import './style/Budget.scss';
import { ReactComponent as MoreIcon } from "../../assets/icons/more.svg";
import { ReactComponent as CardIcon } from "../../assets/icons/card.svg";
import { ReactComponent as FinanceIcon } from "../../assets/icons/finance.svg";
import { ReactComponent as BulbIcon } from "../../assets/icons/bulb.svg";
import { ReactComponent as DownArrow } from "../../assets/icons/back-arrow.svg";
import { ReactComponent as DownloadIcon } from "../../assets/icons/download.svg";

const budgetList = [
    { id: 0, Icon: CardIcon, label: 'EXPENCES', amount: '$11,763.34', percentage: '2.6%', progress: "50", stroke: '#c81e1e', background: '#fde8e8', rotate: '270' },
    { id: 1, Icon: FinanceIcon, label: 'SAVINGS', amount: '$10,974.12', percentage: '12.7%', progress: "5", stroke: '#5850ec', background: '#e5edff', rotate: '90' },
    { id: 2, Icon: BulbIcon, label: 'BILLS', amount: '$1,789.22', percentage: '105.7%', progress: "100", stroke: '#5850ec', background: '#d5f5f6', rotate: '90' },
]


const Budget: React.FC = () => {
    return (
        <div className="budget">
            <div className="budget-top">
                <div>
                    <p className="primary-text">BUDGET</p>
                    <p className="secondary-text">Monthly budget summary</p>
                </div>
                <button className="icon-button top-button">
                    <MoreIcon className="icon" />
                </button>
            </div>

            <p className="summary-text">Last month; you had <strong>223</strong> expense transactions, <strong>12</strong> savings entries and <strong>4</strong> bills. </p>

            {budgetList.map(budget => <div
                key={budget.id}
                className="budget-list"
            >
                <div
                    style={{ backgroundColor: budget.background }}
                    className="icon-wrapper">
                    <budget.Icon stroke={budget.stroke} className="budget-icon" />
                </div>
                <div className="list-center">
                    <p className="label-text">{budget.label}</p>
                    <p className="amount">{budget.amount}</p>
                    <div
                        style={{ backgroundColor: budget.background }}
                        className="progress-bar">
                        <div style={{ backgroundColor: budget.stroke, height: 4, width: `${budget.progress}%` }}></div>
                    </div>
                </div>
                <div className="list-right">
                    <p className="progress-text">{budget.percentage}</p>
                    <DownArrow
                        transform={`rotate(${budget.rotate})`}
                        className="arrow" />
                </div>
            </div>)}

            <p className="secondary-text">Exceeded your personal limit on <strong>bills</strong>! Be careful next month.</p>

            <button className="download-button">
                <DownloadIcon className="download-icon"/>
                <span>Download Summary</span>
            </button>
        </div>

    )
}

export default Budget;