import React from 'react';
import "./style/Finance.scss";

import { ReactComponent as ExportIcon } from "../../assets/icons/export.svg";
import { ReactComponent as ReportIcon } from "../../assets/icons/report.svg";
import { ReactComponent as SettingtIcon } from "../../assets/icons/setting.svg";
import { ReactComponent as MoreIcon } from "../../assets/icons/more.svg";
import Statement from './Statement';
import RecentTransaction from './RecentTransaction';
import Budget from './Budget';

const List = [
    { id: 0, label: "Export", Icon: ExportIcon },
    { id: 1, label: "Reports", Icon: ReportIcon },
    { id: 2, label: "Settings", Icon: SettingtIcon }
]


const Finance: React.FC = () => {
    return (
        <div className="finance-container">

            <div className="top-container">
                <div className="top-right-container">
                    <h2 className="title">Finance dashboard</h2>
                    <span className="sub-title">Keep track of your financial status</span>
                </div>

                <div className="button-container">
                    {List.map(l => <button
                        key={l.id}
                        className="button"
                    >
                        <l.Icon className="icon" />
                        <span className="sub-title button-text">{l.label}</span>
                    </button>)}
                </div>

                <button className="icon-button top-button">
                    <MoreIcon className="icon" />
                </button>
            </div>

            <Statement/>

            <div className="finance-bottom-container">
                <RecentTransaction/>
                <Budget/>
            </div>

        </div>

    )
}

export default Finance;