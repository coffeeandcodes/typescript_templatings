export let options = {
    chart: {
        height: '100%',
        type: "area",
        toolbar: {
            show: false
        },
        sparkline: {
            enabled: true
        }
    },
    
  tooltip: {
    theme: "dark"
},


    dataLabels: {
        enabled: false,
    },
    stroke: {
        width: 2
    },
    colors: ['#a3bffa', '#2E93fA'],
    series: [
        {
            name: "Predicted",
            data: [50, 45, 50, 45, 40, 45, 40, 35, 45, 40, 42, 40, 45, 50, 40, 45, 42, 48, 45, 40, 35, 38, 40, 43, 45, 40, 50, 55, 60, 55]
        },
        {
            name: "Actual",
            data: [25, 22, 20, 25, 20, 15, 20, 25, 35, 20, 22, 30, 32, 30, 20, 25, 22, 38, 35, 20, 15, 28, 20, 23, 35, 20, 30, 35, 40, 35]
        }
    ],
    xaxis: {
        show: false,
        labels: {
            show: false,
        },
        categories: [
            "01 Jan",
            "02 Jan",
            "03 Jan",
            "04 Jan",
            "05 Jan",
            "06 Jan",
            "07 Jan",
            "08 Jan",
            "09 Jan",
            "10 Jan",
            "11 Jan",
            "12 Jan",
            "13 Jan",
            "14 Jan",
            "15 Jan",
            "16 Jan",
            "17 Jan",
            "18 Jan",
            "19 Jan",
            "20 Jan",
            "21 Jan",
            "22 Jan",
            "23 Jan",
            "24 Jan",
            "25 Jan",
            "26 Jan",
            "27 Jan",
            "28 Jan",
            "29 Jan",
            "30 Jan",
        ],
        floating: true,
        tooltip: {
            enabled: false
        },
    },
    yaxis: {
        show: false,
    },
    grid: {
        show: false,
        padding: {
            left: 0,
            right: 0
        }
    },

};