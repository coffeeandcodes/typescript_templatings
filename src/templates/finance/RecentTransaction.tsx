import React from 'react';
import './style/RecentTransaction.scss';

let transactionData = [
    { tId: "528651571NT", date: 'Oct 08, 2019', name: 'Morgan Page', amount: '$1,358.75', status: 'complete' },
    { tId: "421436904YT", date: 'Dec 18, 2019', name: 'Nita Hebert', amount: '-$1,042.82', status: 'complete' },
    { tId: "685377421YT", date: 'Dec 25, 2019', name: 'Marsha Chambers', amount: '$1,828.16', status: 'pending' },
    { tId: "884960091RT", date: 'Nov 29, 2019', name: 'Charmaine Jackson', amount: '$1,647.55', status: 'complete' },
    { tId: "361402213NT", date: 'Nov 24, 2019', name: 'Maura Carey', amount: '-$927.43', status: 'complete' }
]

const RecentTransaction: React.FC = () => {
    return (
        <div className="recent-transaction">
            <div className="transaction-header">
                <p className="primary-text">RECENT TRANSACTIONS</p>
                <p className="secondary-text">1 pending, 4 completed</p>
            </div>

            <div className="overflow-auto">
                <table>
                    <thead>
                        <tr>
                            <th>Transaction ID</th>
                            <th>Date</th>
                            <th>Name</th>
                            <th>Amount</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {transactionData.map(transaction => <tr key={transaction.tId}>
                            <td className="transactionId">{transaction.tId}</td>
                            <td className="td-primary">{transaction.date}</td>
                            <td className="td-primary">{transaction.name}</td>
                            <td className="amount">{transaction.amount}</td>
                            <td>
                                <div className={`transaction-status transaction-status-${transaction.status}`}>
                                    <span className={`circle circle-${transaction.status}`}></span>
                                    <span className="status">{transaction.status}</span>
                                </div>
                            </td>
                        </tr>)}
                    </tbody>
                </table>

                <button className="footer-button">
                    <span>See all transaction</span>
                </button>
            </div>
        </div>

    )
}

export default RecentTransaction;